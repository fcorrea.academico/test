/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hogar
 */
@WebServlet(name = "ImcServlet", urlPatterns = {"/ImcServlet"})
public class ImcServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            //Recuperación de datos desde el formulario
            //en el getparameter va el nombre del name del campo
            double peso = Double.parseDouble(request.getParameter("peso"));
            double estatura = Double.parseDouble(request.getParameter("estatura"));
            int edad = Integer.parseInt(request.getParameter("edad"));

            //Procesar --- Puedo delegar la responsabilidad o hacerlo yo
            double imc = peso / Math.pow(estatura, 2);
            String riesgo = "";

            if (imc < 22 && edad < 45) {
                riesgo = "bajo";
            }
            if ((imc < 22 && edad >= 45) || (imc >= 22 && edad < 45)) {
                riesgo = "medio";
            }
            if (imc >= 22 && edad >= 45) {
                riesgo = "alto";
            }
            
            //pintar valores en el formulario
            request.getSession().setAttribute("imc", imc);
            request.getSession().setAttribute("riesgo", riesgo);
            
            response.sendRedirect("index.jsp");

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
