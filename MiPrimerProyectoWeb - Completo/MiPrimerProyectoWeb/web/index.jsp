<%-- 
    Document   : index
    Created on : 17-12-2020, 17:44:00
    Author     : Hogar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ejercicio IMC</title>
        
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous" />
    <script defer src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>
    </head>
    <body>
        
        <main>
        <div class="container" style="background-color: #fafafa">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <form action="ImcServlet" method="POST">
                        <h4>Calculadora de IMC: Indice de Masa Corporal</h4>
                        <div class="form-group">
                            <label for="peso">Peso en Kg</label>
                            <input type="text" class="form-control" id="peso" name="peso"/>
                        </div>
                        <div class="form-group">
                            <label for="altura">Altura en (m)</label>
                            <input type="text" class="form-control" id="altura" name="estatura"/>
                        </div>
                         <div class="form-group">
                            <label for="edad">Edad</label>
                            <input type="text" class="form-control" id="edad" name="edad"/>
                        </div>
                        <input type="submit" class="btn btn-info" id="calcular">

                        <div class="form-group">
                            <label for="calcular">IMC</label>
                            <input type="text" class="form-control" id="imc" disabled name="imc"  value="${imc}"/>
                        </div>
                        <div class="form-group">
                            <label for="calcular">Lectura</label>
                            <input type="text" class="form-control" id="lectura" disabled name="lectura" value="${riesgo}"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
        
        
    </body>
</html>
